//
// File:    main.scad
// Author:  JFG
// Date:    July 2020
//
// Enclosure for the ErgoJam keyboard, intended for 3D printing.
// All dimensions are in mils (0.001").
// I'd use metric, but 0.75" is the ANSI standard key spacing.
//

// Reset camera on preview/render?
reset_view = 1;
$vpr = reset_view ? [45, 0, 22.5]   : $vpr;
$vpt = reset_view ? [4000, 4000, 0] : $vpt;
$vpd = reset_view ?  20000          : $vpd;

// If you just want to tweak things, start in this file.
include<params.scad>;

// Module definitions
use<switch_cutout.scad>;
use<latch_cutout.scad>;

// TODO: import PCB as STL to create clearance cutouts.

// Un-comment to make right-handed. You could also do this in your slicer software.
//mirror([1,0,0]) // Mirror geometry along X axis.

// Un-comment to get a 2D shape for DXF export to PCB layout software.
//projection() // Project geometry onto X/Y plane.

// Let's build this thing.
difference() {
    // Main body.
    color("grey")
    minkowski() { // Get those sick radiused edges.
        translate([enc_length/2, enc_width/2])
        // Minkowski adds the radius, so subtract here to get expected size.
        linear_extrude(height=enc_thickness-edge_radius, scale=0.98)
        // Scale happens relative to origin, so we need to center our shape, scale it, then move it back.
        translate([-enc_length/2, -enc_width/2])
        difference() {
            // Start with a square.
            square([enc_length, enc_width]);
            
            // Then use these shapes like cookie cutters to cut chunks out.
            translate([6125, 3000]) square([enc_length, enc_width] * a_bunch);
            translate([6125, 0]) rotate(thumb_ang) square([enc_length, enc_width] * a_bunch);
            translate([6125, 3000]) rotate(thumb_ang) square([enc_length, enc_width] * a_bunch);
            translate([-250, 2700]) rotate([0,0,-90] + thumb_ang) square([enc_length, enc_width] * a_bunch);
        };
        // This will be added at all exterior points, resulting in a body with rounded edges
        // and larger in every direction by edge_radius.
        sphere(edge_radius);
    };
    
    // None of the following cutouts get radiused edges.
    
    // Flatten the bottom.
    color("red")
    translate([-edge_radius, -edge_radius, -edge_radius] * a_bunch)
    cube([enc_length, enc_width, edge_radius] * a_bunch);
    
    // Cut out finger keyswitch bodies.
    color("yellow")
    translate([1750, 2875, -1]) // Drop it down by 1 so no 2D plane is left.
    linear_extrude(enc_thickness * a_bunch) // Use a ratio to guarantee it's always tall enough.
    for(this_loc = finger_locs) {
        switch_cutout(size=switch_size, loc=this_loc);
    };
    
    // Cut out finger key latches.
    color("orange")
    translate([1750, 2875, -latch_depth])
    linear_extrude(base_thickness)
    for(this_loc = finger_locs) {
        latch_cutout(size=[switch_size[1]+50, 250], loc=this_loc);
    };

    // Cut out finger key caps.
    color("lightgreen")
    translate([1750, 2875, base_thickness])
    for(i = [0:len(finger_locs)-1]) {
        linear_extrude(enc_thickness * a_bunch)
        switch_cutout(size=finger_sizes[i], loc=finger_locs[i]);
    };

    // Cut out thumb keys.
    color("yellow")
    translate([5125, 1125, -1])
    rotate(thumb_ang)
    linear_extrude(enc_thickness * a_bunch)
    for(this_loc = thumb_locs) {
        switch_cutout(size=switch_size, loc=this_loc);
    };

    // Cut out thumb key latches.
    color("orange")
    translate([5125, 1125, -latch_depth])
    rotate(thumb_ang)
    linear_extrude(base_thickness)
    for(this_loc = thumb_locs) {
        latch_cutout(size=[switch_size[1]+50, 250], loc=this_loc);
    };
    
    // Cut out thumb key caps.
    color("lightgreen")
    translate([5125, 1125, base_thickness])
    rotate(thumb_ang)
    for(i = [0:len(thumb_locs)-1]) {
        linear_extrude(enc_thickness * a_bunch)
        switch_cutout(size=thumb_sizes[i], loc=thumb_locs[i]);
    };
    
    // TODO: Subtract imported PCB model to create pockets for components.
};

// All done!
