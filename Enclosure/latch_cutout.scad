// Cutout for a single set of recesses for switch latches.
module latch_cutout(size=[0,0], loc=[0,0]) {
    translate(loc)
    union() {
        square(size, center=true);
        rotate([0,0,90]) square(size, center=true);
    };    
};