// Cutout for a single switch.
module switch_cutout(size=[0,0], loc=[0,0]) {
    translate(loc)
    square(size, center=true);
};