//
// File:    ErgoJamEnc.scad
// Author:  JFG
// Date:    Aug 2020
//
// Parameter file for ErgoJam keyboard.
//

// Curvature detail. Don't mess until it's time to export.
$fa = 12;
$fs =  2;
$fn =  0;

// Keyswitch parameters.
switch_size = [556, 556]; // Cutout size for switch body.
latch_depth =  62; // Space between flange and latch on keyswitch.
v_stag      =  250; // Amount of vertical stagger between adjacent columns.
v_step      =  750; // Vertical key spacing.
h_step      =  750; // Horizontal key spacing.
thumb_ang   = [0,0,-22.5]; // Thumb cluster angle.

// Enclosure parameters.
enc_length     = 7250;
enc_width      = 7125;
edge_radius    = 125; // Radius applied to enclosure outside edges.
base_thickness = 200;
pocket_depth   = 375;
enc_thickness  = base_thickness + pocket_depth;

// Key cap parameters.
cap_width =  755; // They're actually a little narrower. This includes clearance.
cap_1x1   = [cap_width,     cap_width];
cap_15x1  = [1.5*cap_width, cap_width];
cap_1x15  = [cap_width,     1.5*cap_width];
cap_1x2   = [cap_width,     2*cap_width];

// Multiplier to guarantee some things are always Plenty Big Enough.
// I find it more understandable when expressed in English.
a_bunch = 2;

// Define relative location for each switch.
finger_locs = [[-(h_step+187.5), 4*v_step+2*v_stag], [0*h_step, 4*v_step+2*v_stag], [1*h_step, 4*v_step+1*v_stag], [2*h_step, 4*v_step+2*v_stag], [3*h_step, 4*v_step+1*v_stag], [4*h_step, 4*v_step+0*v_stag], [5*h_step, 4*v_step+0*v_stag], 
               [-(h_step+187.5), 3*v_step+2*v_stag], [0*h_step, 3*v_step+2*v_stag], [1*h_step, 3*v_step+1*v_stag], [2*h_step, 3*v_step+2*v_stag], [3*h_step, 3*v_step+1*v_stag], [4*h_step, 3*v_step+0*v_stag], [5*h_step, 3*v_step+0*v_stag-187.5], 
               [-(h_step+187.5), 2*v_step+2*v_stag], [0*h_step, 2*v_step+2*v_stag], [1*h_step, 2*v_step+1*v_stag], [2*h_step, 2*v_step+2*v_stag], [3*h_step, 2*v_step+1*v_stag], [4*h_step, 2*v_step+0*v_stag], 
               [-(h_step+187.5), 1*v_step+2*v_stag], [0*h_step, 1*v_step+2*v_stag], [1*h_step, 1*v_step+1*v_stag], [2*h_step, 1*v_step+2*v_stag], [3*h_step, 1*v_step+1*v_stag], [4*h_step, 1*v_step+0*v_stag], [5*h_step, 1*v_step+0*v_stag+187.5], 
               [      -1*h_step, 0*v_step+2*v_stag], [0*h_step, 0*v_step+2*v_stag], [1*h_step, 0*v_step+1*v_stag], [2*h_step, 0*v_step+2*v_stag], [3*h_step, 0*v_step+1*v_stag]
              ];

// Define the key cap size used at each of the above locations.
finger_sizes = [cap_15x1, cap_1x1, cap_1x1, cap_1x1, cap_1x1, cap_1x1, cap_1x1,
                cap_15x1, cap_1x1, cap_1x1, cap_1x1, cap_1x1, cap_1x1, cap_1x15,
                cap_15x1, cap_1x1, cap_1x1, cap_1x1, cap_1x1, cap_1x1,
                cap_15x1, cap_1x1, cap_1x1, cap_1x1, cap_1x1, cap_1x1, cap_1x15,
                cap_1x1,  cap_1x1, cap_1x1, cap_1x1, cap_1x1
               ];

// Same thing for the thumb cluster.
thumb_locs = [[0, 1500], [750, 1500], [-750, 375], [0, 375], [750, 750], [750,0]];
thumb_sizes = [cap_1x1,   cap_1x1,     cap_1x2,     cap_1x2,  cap_1x1,    cap_1x1];