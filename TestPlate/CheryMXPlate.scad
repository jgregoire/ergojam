// File:    CherryMXPlate.scad
// Author:  JFG
// Date:    July 2020
//
// Test mounting plate for Cherry MX style keyswitches.
// ErgoDox arrangement tweaked to suit my small hands.
// Units are in mil (0.001 in).
// This is intended to be 3D printed or routed out of
// standard 1.6 mm/62 mil FR-4 PCB stock.
//

// Not needed, but left for reference.
//use <rounded_square.scad>;

v_stag = 250; // Amount of vertical stagger between adjacent columns.
v_step = 750; // Vertical key spacing.
h_step = 750; // Horizontal key spacing.
thumb_ang = [0,0,-22.5]; // Thumb cluster angle
switch_width = 556; // From datasheet for Cherry MX Red.

// Cutout for a single switch
module switch_cutout(size=[switch_width, switch_width], radius=10, loc=[0,0]) {
    translate(loc)
    
    // Radiused corners are not necessary for 3D print and blow up the render filesize.
    // Necessary for subtractive manufacturing (routing) i.e. circuit board.
    //rounded_square(dim = size, corners=[radius,radius,radius,radius], center = true);
    square(size, center=true);
};

// Define relative location for each switch.
// The bottom left key used to be at 0,0, but then I shifted that column upwards by 2 stagger steps.
key_locs = [[-(h_step+187.5), 4*v_step+3*v_stag], [0*h_step, 4*v_step+3*v_stag], [1*h_step, 4*v_step+2*v_stag], [2*h_step, 4*v_step+3*v_stag], [3*h_step, 4*v_step+2*v_stag], [4*h_step, 4*v_step+1*v_stag], [5*h_step, 4*v_step+1*v_stag], 
            [-(h_step+187.5), 3*v_step+3*v_stag], [0*h_step, 3*v_step+3*v_stag], [1*h_step, 3*v_step+2*v_stag], [2*h_step, 3*v_step+3*v_stag], [3*h_step, 3*v_step+2*v_stag], [4*h_step, 3*v_step+1*v_stag], [5*h_step, 3*v_step+1*v_stag-187.5], 
            [-(h_step+187.5), 2*v_step+3*v_stag], [0*h_step, 2*v_step+3*v_stag], [1*h_step, 2*v_step+2*v_stag], [2*h_step, 2*v_step+3*v_stag], [3*h_step, 2*v_step+2*v_stag], [4*h_step, 2*v_step+1*v_stag], 
            [-(h_step+187.5), 1*v_step+3*v_stag], [0*h_step, 1*v_step+3*v_stag], [1*h_step, 1*v_step+2*v_stag], [2*h_step, 1*v_step+3*v_stag], [3*h_step, 1*v_step+2*v_stag], [4*h_step, 1*v_step+1*v_stag], [5*h_step, 1*v_step+1*v_stag+187.5], 
            [      -1*h_step, 0*v_step+3*v_stag], [0*h_step, 0*v_step+3*v_stag], [1*h_step, 0*v_step+2*v_stag], [2*h_step, 0*v_step+3*v_stag], [3*h_step, 0*v_step+2*v_stag]
           ];

thumb_locs = [[0, 1500], [750, 1500], [-750, 375], [0, 375], [750, 750], [750,0]];

// Comment out linear_extrude() line to generate 2D shape for DXF export.
// Un-comment to generate 3D body for STL export.
linear_extrude(62) // PCB thickness
difference() {
    // Maximum extents
    square([7000, 6625]);

    // Cut out finger keys
    translate([1500, 2375]) for(this_loc = key_locs) switch_cutout(loc = this_loc);
        
    // Cut out thumb keys
    translate([4875, 875]) rotate(thumb_ang) for(this_loc = thumb_locs) switch_cutout(loc = this_loc);
        
    // Trim edges
    //square([3875, 2125]);
    translate([5750, 2750]) square([2000, 4000]);
    translate([5875, 0]) rotate(thumb_ang) square([3000,3000]);
    translate([5750, 2750]) rotate(thumb_ang) square([3000,3000]);
    translate([0, 2375]) rotate([0,0,-90] + thumb_ang) square([3000,8000]);
    translate([0, 11625]) rotate([0,0,-90] + thumb_ang) square([3000,8000]);
};