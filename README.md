# ErgoJam - DIY Mechanical Keyboard #

An adaptation of the ErgoDox open source mechanical keyboard to fit this developer's smallish hands better.

### Improvements ###

* Increased amount of vertical stagger bewteen columns.
* Moved thumb cluster.
* Configurable and 3D-printable enclosure.
* PCB with sockets for modularity.
* No power delivered through TRRS cable until both ends fully inserted.
* Compatible with ErgoDoxEZ Oryx config tool and Wally firmware flashing tool.
* No tilt/tent hardware required. Just customize the bottom half of the enclosure.

### Requirements ###

* OpenSCAD.
* Cura or other slicer software.
* 3D printer.
* Altium Designer. Sorry.
* 2-layer PCB fabrication. [Insert link to OSHPark.]
* Soldering iron.

### What Do? ###

* Clone repository.
* Use the .stl files to print the enclosure.
* Use the Gerber files to have PCBs fabricated.
* Order and install the parts listed in the Bill of Materials.
* Configure with QMK or Oryx.

### Long Term Goals ###

* Port PCB design to KiCAD. Need to learn KiCAD first.
* LED backlighting support.
* Thorough instructions.